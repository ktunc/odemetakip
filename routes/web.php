<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Storage;

//Route::get('/', 'OdemetakipController@index');


Route::get('/download', 'OdemetakipController@download');
//Route::get('/download/{filename?}', function (){
//    return Storage::download($filename);
//});


Route::group(['middleware' => 'auth'], function (){
    Route::get('/', 'OdemetakipController@index');
    Route::get('/odemetakip', 'OdemetakipController@index');
    Route::get('/odemetakip/index', 'OdemetakipController@index');
    Route::post('/odemetakip/ajaxodemegetir', 'OdemetakipController@ajaxodemegetir');
    Route::post('/odemetakip/ajaxodemesil', 'OdemetakipController@ajaxodemesil');
    Route::post('/odemetakip/kaydet', 'OdemetakipController@kaydet');
});

//Route::get('/odemetakip', function () {
//    return view('odemetakip.index');
//});
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

