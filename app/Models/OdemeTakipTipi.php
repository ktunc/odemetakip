<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OdemeTakipTipi extends Model
{
    protected $table = 'odeme_takip_tipi';
}
