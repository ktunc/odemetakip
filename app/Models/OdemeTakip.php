<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OdemeTakip extends Model
{
    protected $table = 'odeme_takip';

    public function odemetipi(){
        return $this->hasOne('App\Models\OdemeTakipTipi', 'id', 'tip_id');
    }
}
