<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Models\OdemeTakip;
use App\Models\OdemeTakipTipi;

class OdemetakipController extends Controller
{
    //

    public function index(){
        $odemeler = OdemeTakip::all()->sortByDesc('tarih');
        $odetipleri = OdemeTakipTipi::all();
        $odemetipleriselect = '<option value="0">Seçiniz</option>';
        foreach ($odetipleri as $row){
            $odemetipleriselect .= '<option value="'.$row->id.'">'.$row->tip.'</option>';
        }

//        $data = DB::table('odeme_takip')->where('id',1)->get();
        return view('odemetakip.index', ['odemeler'=>$odemeler, 'odemetipleri'=>$odetipleri, 'odemetipleriselect'=>$odemetipleriselect]);
//        return Storage::download('otfolder/Jy3hG601M5ZXdon5yjVOO4ZHoBKm1ezL9KgMeOKF.png');
    }

    public function kaydet(Request $request){
//        $path = $request->file('dosya')->store('odemetakip');
//        Storage::delete('odemetakip/GXXHe6S7CDjdwqrnrKtcc28oscqJgZU4UoJ8anpK.png');
        if ($request->isMethod('post')) {
            if($request->odeme_id != 0){
                $odemetakip = OdemeTakip::find($request->odeme_id);
            } else{
                $odemetakip = new OdemeTakip;
            }

            if($request->hasFile('dosya')){
                $request->validate([
                    'dosya' => 'image|mimes:jpeg,png,jpg,gif,svg'
                ]);
                $path = $request->file('dosya')->store('otfolder');
//                $path = Storage::disk('Y')->putFile('otfolder', $request->file('dosya'));
                $odemetakip->dosya = $path;
            }

            $odemetakip->aciklama = $request->aciklama;
            $odemetakip->tarih = $request->tarih;
            $odemetakip->fiyat = $request->fiyat;
            $odemetakip->tip_id = $request->tip_id;

            $durum = $odemetakip->save();
            return response()->json($durum);
        } else {
            return response()->json(false);
        }
    }

    public function ajaxodemegetir(Request $request){
        if ($request->isMethod('post')) {
            if(!Auth::check()){
                return response()->json(false);
            }
            if($request->has('odeme_id')){
                $odemetakip = OdemeTakip::find($request->odeme_id);
                if($odemetakip){
                    return response()->json($odemetakip);
                } else{
                    return response()->json(false);
                }
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function ajaxodemesil(Request $request){
        if ($request->isMethod('post')) {
            if(!Auth::check()){
                return response()->json(false);
            }
            if($request->has('odeme_id')){
                $odemetakip = OdemeTakip::find($request->odeme_id);
                if($odemetakip){
                    if(!empty($odemetakip->dosya)){
                        Storage::delete($odemetakip->dosya);
                    }
                    $durum = $odemetakip->delete();
                    if($durum){
                        return response()->json(true);
                    }else{
                        return response()->json(false);
                    }
                } else{
                    return response()->json(false);
                }
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function download(Request $request){
        return Storage::disk('Y')->download($request->dl);
    }
}
