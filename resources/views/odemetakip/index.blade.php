@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-right" onclick="FuncOdemeDuzenle(0)"><i class="fas fa-plus"></i> Ödeme Ekle</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th width="5%">ID</th>
                        <th width="15%">Ödeme Tipi</th>
                        <th>Açıklama</th>
                        <th width="10%">Fiyat</th>
                        <th width="5%">Dosya</th>
                        <th width="10%">Tarih</th>
                        <th width="10%">İşlem</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($odemeler as $odeme)
                        <tr>
                            <td class="text-center">{{$odeme->id}}</td>
                            <td class="text-center">{{$odeme->odemetipi->tip}}</td>
                            <td>{{$odeme->aciklama}}</td>
                            <td><i class="fas fa-lira-sign"></i> {{number_format($odeme->fiyat,2, ',', '.')}}</td>
                            <td class="text-center">
                                @if(!empty($odeme->dosya))
                                    <a target="_blank" href="{{(\Illuminate\Support\Facades\Storage::url($odeme->dosya))}}"><i class="fas fa-download"></i></a>
                                @endif
                            </td>
                            <td class="text-center">{{$odeme->tarih}}</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-warning btn-sm" onclick="FuncOdemeDuzenle({{$odeme->id}})"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="FuncOdemeSil({{$odeme->id}})"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<script type="text/javascript">
var odemeekledialog;
$(document).ready(function () {
    $(document).on('click','button#OdemeKaydet',function () {
        if($('form#OdemeForm select[name="tip_id"]').val() == 0){
            $.alert({
                theme:'modern',
                columnClass: 'medium',
                closeIcon:false,
                icon:'fas fa-exclamation',
                type:'orange',
                title:'Ödemet Tipi Seçiniz',
                content:''
            });
        }else if($('form#OdemeForm input[name="fiyat"]').val() == '' || $('form#OdemeForm input[name="fiyat"]').val() == 0){
            $.alert({
                theme:'modern',
                columnClass: 'medium',
                closeIcon:false,
                icon:'fas fa-exclamation',
                type:'orange',
                title:'Fiyat Giriniz',
                content:''
            });
        }else if($('form#OdemeForm input[name="tarih"]').val() == ''){
            $.alert({
                theme:'modern',
                columnClass: 'medium',
                closeIcon:false,
                icon:'fas fa-exclamation',
                type:'orange',
                title:'Tarih Seçiniz',
                content:''
            });
        }else {
            var formdata = new FormData($('form#OdemeForm').get(0));
            $.ajax({
                type:'POST',
                url:"{{url('/odemetakip/kaydet')}}",
                data:formdata,
                dataType:'json',
                processData: false,
                contentType: false,
                cache:false,
                beforeSend:function () {
                    odemeekledialog.showLoading(false);
                }
            }).done(function (data) {
                if(data){
                    window.location.reload();
                }else{
                    $.alert({
                        theme:'modern',
                        columnClass: 'medium',
                        closeIcon:false,
                        icon:'fas fa-close',
                        type:'red',
                        title:'Bir hata meydama geldi.',
                        content:'',
                        buttons:{
                            tamam:{
                                text:'Tamam',
                                action:function () {
                                    odemeekledialog.hideLoading();
                                }
                            }
                        }
                    });
                }
            }).fail(function (data) {
                $.alert({
                    theme:'modern',
                    columnClass: 'medium',
                    closeIcon:false,
                    icon:'fas fa-close',
                    type:'red',
                    title:'Bir hata meydama geldi.',
                    content:'',
                    buttons:{
                        tamam:{
                            text:'Tamam',
                            action:function () {
                                odemeekledialog.hideLoading();
                            }
                        }
                    }
                });
            });
        }
    });
});

function FuncOdemeDuzenle(odeme_id){
    if(odeme_id > 0){
        $.ajax({
            type:'POST',
            url:"{{url('/odemetakip/ajaxodemegetir')}}",
            data:{'odeme_id':odeme_id},
            dataType:'json'
        }).done(function (data) {
            if(data){
                odemeekledialog = $.dialog({
                    columnClass: 'medium',
                    closeIcon:false,
                    type:'blue',
                    title:'Ödeme Ekle',
                    content:'<div class="container"><form id="OdemeForm">' +
                    '<input type="hidden" name="odeme_id" value="'+data['id']+'"/>'+
                    '<div class="form-group row">\n' +
                    '    <label for="tip_id" class="col-3 col-form-label text-right">Ödeme Tipi:</label>\n' +
                    '    <div class="col-9">\n' +
                    '      <select name="tip_id" class="form-control"><?=$odemetipleriselect?></select>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '<div class="form-group row">\n' +
                    '    <label for="aciklama" class="col-3 col-form-label text-right">Açıklama:</label>\n' +
                    '    <div class="col-9">\n' +
                    '      <textarea name="aciklama" rows="5" class="form-control">'+data['aciklama']+'</textarea>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '<div class="form-group row">\n' +
                    '    <label for="fiyat" class="col-3 col-form-label text-right">Fiyat:</label>\n' +
                    '    <div class="col-9">\n' +
                    '      <input type="number" class="form-control" name="fiyat" value="'+data['fiyat']+'" autocomplete="off">\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '<div class="form-group row">\n' +
                    '    <label for="tarih" class="col-3 col-form-label text-right">Tarih:</label>\n' +
                    '    <div class="col-9">\n' +
                    '      <input type="text" class="form-control" name="tarih" value="'+data['tarih']+'" autocomplete="off">\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '<div class="form-group row">\n' +
                    '    <label for="dosya" class="col-3 col-form-label text-right">Dosya:</label>\n' +
                    '    <div class="col-9">\n' +
                    '      <input type="file" class="form-control-file" name="dosya">\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '<div class="form-group row">\n' +
                    '    <div class="col-6"><button type="button" class="btn btn-sm btn-danger" onclick="odemeekledialog.close();">Kapat</button></div>\n' +
                    '    <div class="col-6 text-right"><button type="button" class="btn btn-sm btn-success" id="OdemeKaydet">Kaydet</button></div>\n' +
                    '  </div>' +
                    '</form></div>',
                    onContentReady:function () {
                        $('form#OdemeForm select[name="tip_id"]').val(data['tip_id']);
                        $('form#OdemeForm input[name="tarih"]').datepicker({
                            format:'yyyy-mm-dd',
                            autoclose:true,
                            language:'tr',
                            todayBtn:'linked',
                            todayHighlight:true
                        });
                    }
                });
            }else{
                console.log(data);
            }
        }).fail(function (data) {

        });
    }else {
        odemeekledialog = $.dialog({
            columnClass: 'medium',
            closeIcon:false,
            type:'blue',
            title:'Ödeme Ekle',
            content:'<div class="container"><form id="OdemeForm">' +
            '<input type="hidden" name="odeme_id" value="'+odeme_id+'"/>'+
            '<div class="form-group row">\n' +
            '    <label for="tip_id" class="col-3 col-form-label text-right">Ödeme Tipi:</label>\n' +
            '    <div class="col-9">\n' +
            '      <select name="tip_id" class="form-control"><?=$odemetipleriselect?></select>\n' +
            '    </div>\n' +
            '  </div>' +
            '<div class="form-group row">\n' +
            '    <label for="aciklama" class="col-3 col-form-label text-right">Açıklama:</label>\n' +
            '    <div class="col-9">\n' +
            '      <textarea name="aciklama" rows="5" class="form-control"></textarea>\n' +
            '    </div>\n' +
            '  </div>' +
            '<div class="form-group row">\n' +
            '    <label for="fiyat" class="col-3 col-form-label text-right">Fiyat:</label>\n' +
            '    <div class="col-9">\n' +
            '      <input type="number" class="form-control" name="fiyat" autocomplete="off">\n' +
            '    </div>\n' +
            '  </div>' +
            '<div class="form-group row">\n' +
            '    <label for="tarih" class="col-3 col-form-label text-right">Tarih:</label>\n' +
            '    <div class="col-9">\n' +
            '      <input type="text" class="form-control" name="tarih" autocomplete="off">\n' +
            '    </div>\n' +
            '  </div>' +
            '<div class="form-group row">\n' +
            '    <label for="dosya" class="col-3 col-form-label text-right">Dosya:</label>\n' +
            '    <div class="col-9">\n' +
            '      <input type="file" class="form-control-file" name="dosya">\n' +
            '    </div>\n' +
            '  </div>' +
            '<div class="form-group row">\n' +
            '    <div class="col-6"><button type="button" class="btn btn-sm btn-danger" onclick="odemeekledialog.close();">Kapat</button></div>\n' +
            '    <div class="col-6 text-right"><button type="button" class="btn btn-sm btn-success" id="OdemeKaydet">Kaydet</button></div>\n' +
            '  </div>' +
            '</form></div>',
            onContentReady:function () {
                $('form#OdemeForm input[name="tarih"]').datepicker({
                    format:'yyyy-mm-dd',
                    autoclose:true,
                    language:'tr',
                    todayBtn:'linked',
                    todayHighlight:true
                });
            }
        });
    }
}

function FuncOdemeSil(odeme_id) {
    $.confirm({
        theme:'modern',
        columnClass: 'medium',
        closeIcon:false,
        icon:'fas fa-question',
        type:'orange',
        title:'Ödemeyi Silmek İstediğinizden Emin Misiniz?',
        content:'',
        buttons:{
            vazgec:{
                text:'Vazgeç'
            },
            sil:{
                text:'SİL',
                btnClass:'btn-danger',
                action:function () {
                    $.ajax({
                        type:'POST',
                        url:"{{url('/odemetakip/ajaxodemesil')}}",
                        data:{'odeme_id':odeme_id},
                        dataType:'json'
                    }).done(function (data) {
                        if(data){
                            window.location.reload();
                        }else{
                            $.alert({
                                theme:'modern',
                                columnClass: 'medium',
                                closeIcon:false,
                                icon:'fas fa-close',
                                type:'red',
                                title:'Silme İşlemi Başarısız',
                                content:'',
                                buttons:{
                                    tamam:{
                                        text:'Tamam',
                                        action:function () {
                                            window.location.reload();
                                        }
                                    }
                                }
                            });
                        }
                    }).fail(function () {
                        $.alert({
                            theme:'modern',
                            columnClass: 'medium',
                            closeIcon:false,
                            icon:'fas fa-close',
                            type:'red',
                            title:'Silme İşlemi Başarısız',
                            content:'',
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        window.location.reload();
                                    }
                                }
                            }
                        });
                    });
                }
            }
        }
    });
}
</script>

@endsection
