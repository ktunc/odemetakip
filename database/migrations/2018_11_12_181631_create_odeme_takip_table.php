<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdemeTakipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odeme_takip', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('tip_id');
            $table->mediumText('aciklama')->nullable();
            $table->mediumText('dosya');
            $table->double('fiyat');
            $table->dateTime('tarih');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odeme_takip');
    }
}
